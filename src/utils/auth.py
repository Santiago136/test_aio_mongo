from aiohttp.web_exceptions import HTTPUnauthorized

from utils.db_connector import DBConnector


class Authenticator:
    def __init__(self, connector: DBConnector):
        self.__connector = connector

    async def authorize(self, login: str, password: str):
        if login is None or password is None:
            raise HTTPUnauthorized

        user_params = {
            'login': login,
            'password': password,
        }

        user = await self.__connector.find_document(user_params, 'users')
        if not user:
            raise HTTPUnauthorized

        return user['_id']
