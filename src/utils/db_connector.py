from pymongo import MongoClient
from pymongo.errors import DuplicateKeyError

from utils.exceptions import DataBaseError


class DBConnector:

    def __init__(
        self,
        host: str,
        port: int,
        database: str,
        collection: str,
    ):
        self.__client = MongoClient(host, port)
        self.__db = self.__client[database]
        self.__collection = self.__db[collection]

    async def set_collection(self, name: str):
        self.__collection = self.__db[name]

    async def add_document(self, data: dict, collection: str):
        await self.set_collection(collection)
        try:
            self.__collection.save(data)
        except DuplicateKeyError:
            raise DataBaseError('Some of keys are exists')

    async def find_document(self, data: dict, collection: str) -> dict:
        await self.set_collection(collection)
        document = self.__collection.find_one(data)
        if document:
            document = {str(key): str(value) for key, value in document.items()}

        return document
