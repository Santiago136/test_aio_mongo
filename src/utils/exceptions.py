import json

from aiohttp.web_exceptions import HTTPBadRequest, HTTPForbidden


class ValidationError(HTTPBadRequest):
    def __init__(self, message):
        super().__init__()
        self.content_type = 'application/json'
        self.body = json.dumps({'error_msg': message})


class DataBaseError(HTTPForbidden):
    def __init__(self, message):
        super().__init__()
        self.content_type = 'application/json'
        self.body = json.dumps({'error_msg': message})
