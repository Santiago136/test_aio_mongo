from aiohttp import web

from src.urls import routes
from src.utils.db_connector import DBConnector


async def on_startup(application):
    application['mongo_client'] = DBConnector(
        host='localhost',
        port=27017,
        database='aio_mongo_base',
        collection='users',
    )


async def on_shutdown(application):
    application['mongo_client'].close()


if __name__ == '__main__':
    app = web.Application()
    app.add_routes(routes)
    app.on_startup.append(on_startup)
    app.on_shutdown.append(on_shutdown)
    web.run_app(app)
