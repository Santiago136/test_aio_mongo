from aiohttp import web
from aiohttp.web_exceptions import HTTPInternalServerError

from views.base_view import BaseView


class SignUpView(BaseView):
    async def post(self):
        params = await self.request.json()
        self.validate(params)

        await self.request.app['mongo_client'].add_document(
            data=params,
            collection='users'
        )

        db_connector = self.request.app['mongo_client']

        user_params = {
            'login': params['login'],
            'password': params['password'],
        }

        user = await db_connector.find_document(user_params, 'users')
        if user is None:
            raise HTTPInternalServerError()

        return web.json_response(user)
