import logging

import schemas

from aiohttp import web
from json_payload_validator import validate as json_schema_validate

from utils.auth import Authenticator
from utils.exceptions import ValidationError


class BaseView(web.View):
    def __init__(self, request: web.Request):
        super().__init__(request)
        self._schema = getattr(schemas, self.__class__.__name__)
        self._logger = logging.getLogger(self.__class__.__name__)
        self._connector = request.app['mongo_client']
        self._auth = Authenticator(self._connector)

    def validate(self, params):
        error = json_schema_validate(params, self._schema)
        if error:
            raise ValidationError(error)
