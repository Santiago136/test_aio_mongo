from aiohttp import web
from bson import ObjectId

from views.base_view import BaseView


class GetItemParentsView(BaseView):
    async def post(self):
        params = await self.request.json()
        self.validate(params)

        await self._auth.authorize(
            login=params['login'],
            password=params['password']
        )

        document = await self.request.app['mongo_client'].find_document(
            data={'text': params['text']},
            collection='data'
        )

        parents = []
        await self._find_items_parents(document, parents)

        return web.json_response(parents)

    async def _find_items_parents(self, item: dict, parents: list):
        if item.get('parent_id'):
            parent_item = await self.request.app['mongo_client'].find_document(
                data={'_id': ObjectId(item['parent_id'])},
                collection='data'
            )

            if parent_item:
                parents.append(parent_item)
                await self._find_items_parents(parent_item, parents)

        return parents
