from aiohttp import web

from views.base_view import BaseView


class InsertItemView(BaseView):

    async def post(self):
        params = await self.request.json()
        self.validate(params)

        await self._auth.authorize(
            login=params['login'],
            password=params['password']
        )
        await self.request.app['mongo_client'].add_document(
            data=params['item'],
            collection='data'
        )

        document = await self.request.app['mongo_client'].find_document(
            data=params['item'],
            collection='data'
        )

        return web.json_response(document)
