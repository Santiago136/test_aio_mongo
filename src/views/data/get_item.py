from aiohttp import web

from views.base_view import BaseView


class GetItemView(BaseView):
    async def post(self):
        params = await self.request.json()
        self.validate(params)

        await self._auth.authorize(
            login=params['login'],
            password=params['password']
        )

        document = await self.request.app['mongo_client'].find_document(
            data={'text': params['text']},
            collection='data'
        )

        return web.json_response(document)
