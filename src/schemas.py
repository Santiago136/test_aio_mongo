login = {
    'type': 'string',
    'minLength': 5,
    'maxLength': 20,
    'message': 'must be string and have length: 5-20'
}

password = {
    'type': 'string',
    'minLength': 5,
    'maxLength': 20,
    'message': 'must be string and have length: 5-20'
}

InsertItemView = {
    'type': 'object',
    'properties': {
        'login': login,
        'password': password,
        'item': {
            'type': 'object',
            'properties': {
                'text': {'type': 'string'},
                'parent_id': {'type': 'string'}
            },
            'required': ['text']
        }
    },
    'required': ['login', 'password', 'item'],
}

SignUpView = {
    'type': 'object',
    'properties': {
        'login': login,
        'password': password,
    },
    'required': ['login', 'password'],
}

GetItemView = {
    'type': 'object',
    'properties': {
        'login': login,
        'password': password,
        'text': {'type': 'string'},
    },
    'required': ['login', 'password', 'text'],
}

GetItemParentsView = {
    'type': 'object',
    'properties': {
        'login': login,
        'password': password,
        'text': {'type': 'string'},
    },
    'required': ['login', 'password', 'text'],
}