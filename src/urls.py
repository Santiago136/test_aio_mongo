from aiohttp import web

from views.data.get_item import GetItemView
from views.data.get_item_parents import GetItemParentsView
from views.data.insert_item import InsertItemView
from views.users.sign_up import SignUpView

routes = [
    web.view('/sign_up', SignUpView),
    web.view('/insert_item', InsertItemView),
    web.view('/get_item', GetItemView),
    web.view('/get_item_parent', GetItemParentsView)
]
