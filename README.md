# Aiohttp + MongoDB test project

# install and test:
 * ```mongorestore --db aio_mongo_base ~../aio_mongo/mongo_dump```
 * start mongodb
 * ```pip install -r  requirements.txt```
 * ```python app.py``` 
 * export Postman request to your app and test
 